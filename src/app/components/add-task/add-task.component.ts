import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Task } from 'src/app/Task';
import { UiService } from 'src/app/services/ui.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css'],
})
export class AddTaskComponent implements OnInit {
  @Output() onAddTask = new EventEmitter<Task>();
  subscription: Subscription;
  visible: boolean = false;
  text!: string;
  day!: string;
  reminder?: boolean = false;

  constructor(private uiService: UiService) {
    this.subscription = uiService
      .onToggle()
      .subscribe((value) => (this.visible = value));
  }

  ngOnInit(): void {}

  onAddTaskToggled() {
    this.visible = !this.visible;
  }

  onSubmit() {
    if (!this.text) {
      alert('Please provide text');
      return;
    }
    const task: Task = {
      text: this.text,
      day: this.day,
      reminder: this.reminder || false,
    };
    this.onAddTask.emit(task);
    this.text = '';
    this.day = '';
    this.reminder = false;
  }
}
